from collections import defaultdict
import MySQLdb
import cPickle as pk

# Establish the connection
cnx = { 'host': 'navjot.ccqolheuurhr.us-east-1.rds.amazonaws.com','user':'navjot','password': 'navjot2101','db': 'navjot'}
conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], cnx['db'])
cursor = conn.cursor()

# Get the pids from the production table
query = "SELECT pid FROM navjot.prod"
cursor.execute(query)
row = cursor.fetchall()
pids = [item[0] for item in row]
unique_pid = list(set(pids))

# Now extract the datasets
data = defaultdict(list)
for (cntr, item) in enumerate(unique_pid):
    data['padID'].append(item)
    query = "SELECT oil,wor,proddays FROM prod WHERE pid=\'"+item+"\'"
    cursor.execute(query)
    row = cursor.fetchall()
    data['oil'].append( [i[0] for i in row] )
    data['wor'].append( [i[1] for i in row] )
    data['days'].append( [i[2] for i in row] )
    query = "SELECT pooldesc,bottomx,bottomy,initialdate FROM lbu_master WHERE pid=\'"+item+"\' LIMIT 1"
    cursor.execute(query)
    row = cursor.fetchone()
    data['Xpos'].append(row[1] if row is not None else None)
    data['Ypos'].append(row[2] if row is not None else None)
    data['poolID'].append(row[0] if row is not None else None)
    data['year'].append(row[3] if row is not None else None)
    print str(cntr)+" / "+str(len(unique_pid))

pk.dump( data, open( "dataSQL.p", "wb" ) )
cursor.close()
conn.close()