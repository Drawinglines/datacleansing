import pandas as pd
from os import listdir
from os.path import isfile, join

def readFiles(name): 
    ''' This function reads the file with the given name and creates a dictionary.  
        The output has two fields: 'time series' and 'static'.
    '''
    df = pd.read_csv(name, index_col=0)
    data =  df[['date', 'oil', 'wor', 'proddays', 'fop']]
    if len(df.index) > 0:
	    static = {'PadID':df['pid'][1], 'xy':[df['bottomx'][1], df['bottomy'][1]], 'initYear':df['initial_year'][1],  'pool':df['pooldesc'][1], 'nData':len(df.index)}
    else:
        static = {'nData':len(df.index)}
    return {'ts':data, 'static':static}

# Main script
if __name__ == "__main__":
    mypath = '/data/Forecasting Project/Cleaned Tables/New_All_LBU/'
    files = [ f for f in listdir(mypath) if isfile(join(mypath,f)) ]
    
    data = {}
    for f in files:
        out = readFiles(join(mypath,f))
        data[f] = out
    
    print len(data)
    
    # Pickling
    import cPickle as pk
        
    pk.dump( data, open( "data.p", "wb" ) )


