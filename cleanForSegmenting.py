import pandas as pd
import numpy as np
from os import listdir
from os.path import isfile, join
import csv

def readFiles(name): 
    ''' This function reads the file with the given name and creates a dictionary.  
        The output has two fields: 'time series' and 'static'.
    '''
    df = pd.read_csv(name, index_col=0)
    data =  df[['oil', 'proddays', 'wor', 'wtrinj', 'date']]
    if len(df.index) > 0:
        # 'PadID':, 'x':, 'y':, 'initYear':,'pool':, 'nData':
        static = str(df['pid'][1]) + ',' + str(df['bottomx'][1]) + ',' + str(df['bottomy'][1]) + ',' + str(df['initial_year'][1]) + ',' + str(df['pooldesc'][1]) + ',' + str(len(df.index))
    else:
        static = {'nData':len(df.index)}
    return (data, static)

def processName(name):
    temp = name.split('_')
    return temp[0]+','+temp[1]

# Main script
if __name__ == "__main__":
	mypath = '/data/Forecasting Project/Cleaned Tables/6.21_All_LBU/'
	files = [ f for f in listdir(mypath) if isfile(join(mypath,f)) ]
	
	worFile = open("./data/wor4.csv",'wb')
	worWriter = csv.writer(worFile, delimiter=',')
	oilFile = open("./data/oil4.csv",'wb')
	oilWriter = csv.writer(oilFile, delimiter=',')
	waterFile = open("./data/water4.csv",'wb')
	waterWriter = csv.writer(waterFile, delimiter=',')
	daysonFile = open("./data/days4.csv",'wb')
	daysonWriter = csv.writer(daysonFile, delimiter=',')
	dateFile = open("./data/date4.csv", 'wb')
	dateWriter = csv.writer(dateFile, delimiter=',')
	nameFile = open("./data/names4.csv",'wb')
	
	counter = 1
	for f in files:
		(data, static) = readFiles(join(mypath,f))
		if len(data.index) > 100:
			nameFile.write(processName(f)+ ',' + static+'\n')
			worWriter.writerow(data['wor'].values.transpose())
			waterWriter.writerow(data['wtrinj'].values.transpose())
			oilWriter.writerow(data['oil'].values.transpose())
			daysonWriter.writerow(data['proddays'].values.transpose())
			dateWriter.writerow(data['date'].values.transpose())
			print counter
			counter += 1
			
	worFile.close()
	oilFile.close()
	dateFile.close()
	daysonFile.close()
	waterFile.close()
	nameFile.close()
